//copy from Beng Teck via slack

module.exports = function(conn, Sequelize) {
  var tasks=  conn.define('tasks', {
      task_id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          primaryKey: true,
          autoIncrement: false
      },
      due_date: {
          type: Sequelize.DATE,
          allowNull: false
      },
      task_description: {
          type: Sequelize.STRING,
          allowNull: false
      },
      category: {
          type: Sequelize.STRING,
          allowNull: false
      },
      status: {
          type: Sequelize.ENUM('Pending','Done'),
          allowNull: false
      }
  }, {
      tableName: 'tasks',
      timestamps: false,
/*      hooks: {
          beforeValidate: function(tasks){
              console.log("before validate tasks");
              console.log(tasks.task_description);
          },
          afterValidate: function(tasks){
              console.log("after validate tasks");
          },
          beforeCreate: function(tasks){
              console.log("before create tasks");
              console.log("get old records & update old record column on the audit trail table!");
          },
          afterCreate: function(tasks){
              console.log("after create tasks");
              console.log("update the audit trail new update column !");
             
          },
      }*/
  });
  return tasks;
};